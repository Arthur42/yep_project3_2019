<link href="css/header.css" rel="stylesheet" type="text/css" >

<div class="header" id="header">
    <img class="header-logo" src="images/fast-food.png"></img>
    <div class="header-text"><p>Burger Gaming</p></div>
</div>
<div class="navbar" id="navbar">
        <ul class="begin-tab">
            <li><a href="/yep_project3_2019/" <?php if ($currentPage === 'Home') {echo 'class="active"';} ?>>Accueil</a></li>
            <li><a href="/yep_project3_2019/" <?php if ($currentPage === 'Catalog') {echo 'class="active"';} ?>>Jeux</a></li>
        </ul>
        <ul class="end-tab">
            <?php
            if (!isset($_SESSION['user'])) {
                echo '<li><a href="/yep_project3_2019/login"';
                if ($currentPage === 'Login') {echo 'class="active"';}
                echo '>Connexion</a></li>';
                echo '<li><a href="/yep_project3_2019/register"';
                if ($currentPage === 'Register') {echo 'class="active"';}
                echo '>Créer un compte</a></li>';
            } else {
                echo '<li><a href="/yep_project3_2019/Logout"';
                if ($currentPage === 'Logout') {echo 'class="active"';}
                echo '>Déconnection</a></li>';
            } ?>
        </ul>
</div>