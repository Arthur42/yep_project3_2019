<link href="css/game_preview.css" rel="stylesheet" type="text/css" >

<div class="games-preview" id="games-preview" style="margin-top:<?php echo $margin; ?>%;">
    <div class="game-preview" id="game-preview">
    </div>
</div>

<script>
    var numberOfGames = 0;
    var selectGame = 1;
    var animationPlayed = 0;
    function addGame(name, image_path) {
        numberOfGames++;
        var games = document.getElementById("game-preview");
        games.style.width = (numberOfGames * 500) + "px";
        
        console.log("add game : ", name);
        var elem = document.createElement("DIV");
        elem.classList.add("polaroid");
        elem.style.backgroundImage = 'url("' + image_path + '")';
        
        var elemTitle = document.createElement("P");
        elemTitle.innerHTML = name;
        elemTitle.classList.add("game-title");
        elem.appendChild(elemTitle);
        
        document.getElementById("game-preview").appendChild(elem);
    }
    function changeSelectedGame(direction) {
        if (animationPlayed == 0
            && (
                (direction == -1 && selectGame > 1)
                || (direction == 1 && selectGame < numberOfGames)
            )) {
            console.log("test");
            animationPlayed = 1;
            var elem = document.getElementById("game-preview");
            var pos = (selectGame - 1) * 500 * -1;
            var id = setInterval(frame, 0.5);
            function frame() {
                if (pos == (((selectGame - 1 + direction) * -1) * 500)) {
                    animationPlayed = 0;
                    selectGame += direction;
                    clearInterval(id);
                } else {
                    pos += 5 * -1 * direction;
                    var elem = document.getElementById("game-preview");
                    elem.style.marginLeft = pos + 'px';
                }
            }
        }
    }
    addGame("Not Mario", "./images/jeu_exemple.png");
    addGame("Breakout", "./images/breakout_preview.jpg");
    addGame("Asteroid", "./images/jeu_exemple.png");
    addGame("RPG", "./images/rpg_preview.jpg");
</script>