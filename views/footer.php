<link rel="stylesheet" type="text/css" href="css/footer.css">

<div id="footer" class="footer">
    <div class="grid-item">Contact
        <p><a class="link" href="mailto:dyscovery.contact@gmail.com" title="Adresse électronique Dyscovery" target="_blank">dyscovery.contact@gmail.com</a></p>
    </div>
    <div class="grid-item">Qui sommes-nous ?</div>
    <div class="grid-item">Liens utiles
        <p><a href="https://www.ffdys.com/troubles-dys/dyslexie-et-dysorthographie" title="Fédération Française des Dys" target="_blank">FFDys</a></p>
        <p><a href="https://www.inserm.fr/information-en-sante/dossiers-information/troubles-specifiques-apprentissages" title="INSERM sur les dys" target="_blank">INSERM</a></p>
    </div>
</div>