<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('include')
    </head>
    <body>
        <?php $currentPage = 'Catalog'; ?>
        @include('header')
        <link rel="stylesheet" type="text/css" href="css/test.css">
        <link rel="stylesheet" type="text/css" href="css/lightMode.css">
        <script src="{{ asset('js/background.js') }}">
        <div id="main-wrapper">
            <div class="dropdown">
                <button class="dropbtn">Différents jeux</button>
                <ul class="dropdown-content">
                    <li id="dyslexia"><p>dyslexie</p></li>
                    <li><p id="dyso">dysorthographie</p></li>
                    <li id="dysca"><p>dyscalculie</p></li>
                    <li id="dyspra"><p>dyspraxie</p></li>
                    <li id="dyspha"><p>dysphasie</p></li>
                    <li id="dysgra"><p>dysgraphie</p></li>
                </ul>
            </div>
            <div class="content container" id="content">
                <div id="dys-def">
                    <h2>Troubles dys</h2>
                    <p>
                        Dys- est un raccourci de langage pour évoquer une partie ou l'ensemble
                        des troubles d'apprentissage dont le préfixe est « dys » ; on parle de
                        troubles dys.
                    </p>
                </div>
            </div>
            @include('footer')
        </div>
    </body>
    <script src="{{ asset('js/catalog.min.js') }}"></script>
</html>