<?php session_start(); ?>
<?php
$failConnection = false;
if (isset($_POST["email"]) && isset($_POST["password"])) {
    include 'database/database.php';
    $bdd = new Database;
    if ($bdd->userExist($_POST["email"], $_POST["password"])) {
        $_SESSION['user'] = $_POST['email'];
        echo '<script> window.location.href = \'index\' </script>';
    } else {
        $failConnection = true;
    }
}
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <?php include('views\include.php'); ?>
    </head>
    <body>
        <?php $currentPage = 'Login'; ?>
        <link rel="stylesheet" type="text/css" href="css/welcome.css" >
        <link rel="stylesheet" type="text/css" href="css/form.css" >
        <link rel="stylesheet" type="text/css" href="css/lightMode.css">
        <div id="main-wrapper">
            <?php  include('views/header.php'); ?>
            <div class="content" id="content">
                <form action="login" method="POST">
                    <h3>Connexion</h3>
                    <input type="text" name="email" placeholder="email" />
                    <br>
                    <input type="password" name="password" placeholder="password" />
                    <br>
                    <input type="checkbox" name="stayLog" />
                    <label>Rester connecter</label>
                    <br>
                    <input type="submit" value="connexion" />
                    <?php if ($failConnection) {echo '<div>Mauvais Identifiants</div>';} ?>
                </form>
            </div>
            <?php include('views/footer.php'); ?>
        </div>
    </body>
</html>

<script type="text/javascript">
    const [red, green, blue] = [255, 255, 255]
    const [red1, green1, blue1] = [174, 183, 101]
    const content = document.querySelector('.content')

    var dom = document.getElementById('content')

    const viewportHeight = window.innerHeight
    const elementOffsetTop = dom.offsetTop

    var distance = viewportHeight - elementOffsetTop

    var height = document.getElementById('content').getBoundingClientRect().height
    var footer = document.getElementById('footer').getBoundingClientRect().height

    height = height - distance + footer

    window.addEventListener('scroll', () => {
        var y = window.scrollY / height
        const [r, g, b] = [red-(174*y), green-(183*y), blue-(101*y)].map(Math.round)
        dom.style.backgroundImage = `linear-gradient(to bottom, rgb(255, 255, 255), rgb(${r}, ${g}, ${b}))`
    })
</script>
