<?php
session_start();
unset($_SESSION['user']);
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <?php include('views\include.php'); ?>
    </head>
    <body>
        <?php $currentPage = 'Logout'; ?>
        <link rel="stylesheet" type="text/css" href="css/welcome.css" >
        <link rel="stylesheet" type="text/css" href="css/form.css" >
        <link rel="stylesheet" type="text/css" href="css/lightMode.css">
        <div id="main-wrapper">
            <?php  include('views/header.php'); ?>
            <div class="content" id="content">
                <h3>Déconnecter.</h3>
            </div>
            <?php include('views/footer.php'); ?>
        </div>
    </body>
</html>

<script type="text/javascript">
    const [red, green, blue] = [255, 255, 255]
    const [red1, green1, blue1] = [174, 183, 101]
    const content = document.querySelector('.content')

    var dom = document.getElementById('content')

    const viewportHeight = window.innerHeight
    const elementOffsetTop = dom.offsetTop

    var distance = viewportHeight - elementOffsetTop

    var height = document.getElementById('content').getBoundingClientRect().height
    var footer = document.getElementById('footer').getBoundingClientRect().height

    height = height - distance + footer

    window.addEventListener('scroll', () => {
        var y = window.scrollY / height
        const [r, g, b] = [red-(174*y), green-(183*y), blue-(101*y)].map(Math.round)
        dom.style.backgroundImage = `linear-gradient(to bottom, rgb(255, 255, 255), rgb(${r}, ${g}, ${b}))`
    })
</script>
