package com.example.airdyscovery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MainActivity extends AppCompatActivity {

    Button _btnClickMe;
    private WebSocket webSocket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPage1();
        initWebSocket();
    }

    private void initPage1() {
        setContentView(R.layout.activity_main);
        _btnClickMe = (Button) findViewById(R.id.loginButton);

        _btnClickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) findViewById(R.id.loginCode);
                webSocket.send("_mobileConnexion:" + tv.getText().toString());
                initPage2();
                //connect();
            }
        });
    }

    private void initPage2() {
        setContentView(R.layout.activity_main3);
        Button btnLeft = (Button) findViewById(R.id.left);
        Button btnUp = (Button) findViewById(R.id.up);
        Button btnRight = (Button) findViewById(R.id.right);
        Button btnDown = (Button) findViewById(R.id.down);
        Button btnA = (Button) findViewById(R.id.a);
        Button btnB = (Button) findViewById(R.id.b);

        btnLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:left-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:left-up");
                    return true;
                }
                return false;
            }
        });
        btnUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:up-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:up-up");
                    return true;
                }
                return false;
            }
        });
        btnRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:right-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:right-up");
                    return true;
                }
                return false;
            }
        });
        btnDown.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:down-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:down-up");
                    return true;
                }
                return false;
            }
        });
        btnA.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:a-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:a-up");
                    return true;
                }
                return false;
            }
        });
        btnB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    webSocket.send("_mobileButton:b-down");
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    webSocket.send("_mobileButton:b-up");
                    return true;
                }
                return false;
            }
        });
    }

    private void initWebSocket() {
        OkHttpClient client = new OkHttpClient();
        okhttp3.Request request = new okhttp3.Request.Builder().url(((EditText)findViewById(R.id.ip)).getText().toString()).build();
        SocketListener socketListener = new SocketListener(this);
        webSocket = client.newWebSocket(request, socketListener);
    }

    public class SocketListener extends WebSocketListener {
        public MainActivity activity;

        public SocketListener(MainActivity activity) {
            this.activity = activity;
        }

        @Override
        public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            super.onClosed(webSocket, code, reason);
        }

        @Override
        public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            super.onClosing(webSocket, code, reason);
        }

        @Override
        public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable okhttp3.Response response) {
            super.onFailure(webSocket, t, response);
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
            super.onMessage(webSocket, text);
        }

        @Override
        public void onOpen(@NotNull WebSocket webSocket, @NotNull okhttp3.Response response) {
            super.onOpen(webSocket, response);
        }
    }

    private void connect() {
        String url = "http://dyscoverydev.herokuapp.com/api/login";
        JSONObject obj = new JSONObject();
        try {
            obj.put("email", "ab@gmail.com");
            obj.put("password", 1234);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        initPage2();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
}
