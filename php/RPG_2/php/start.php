<script>
var configRPG = {
    type: Phaser.AUTO,
    parent: gRPG,
    width: 320,
    height: 240,
    zoom: 2,
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false // set to true to view debug zones
        }
    },
    scene: [
        BootScene,
        WorldScene,
        BattleScene,
        UIScene
    ]
};
var gameRPG = new Phaser.Game(configRPG);
</script>