<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Turn-Based RPG in Phaser 3</title>        
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
         
        <script>
        

var BootScene = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function BootScene ()
    {
        Phaser.Scene.call(this, { key: 'BootScene' });
    },

    preload: function ()
    {
        // map tiles
        this.load.image('tiles', 'assets/map/spritesheet.png');
        
        // map in json format
        this.load.tilemapTiledJSON('map', 'assets/map/map.json');
        
        // our two characters
        this.load.spritesheet('player', 'assets/RPG_assets.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('player2', 'assets/RPG_assets.png', { frameWidth: 16, frameHeight: 16 });
    },

    create: function ()
    {
        // start the WorldScene
        this.scene.start('WorldScene');
    }
});

var WorldScene = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function WorldScene ()
    {
        Phaser.Scene.call(this, { key: 'WorldScene' });
    },

    preload: function ()
    {
        
    },

    create: function ()
    {
        // create the map
        var map = this.make.tilemap({ key: 'map' });
        
        // first parameter is the name of the tilemap in tiled
        var tiles = map.addTilesetImage('spritesheet', 'tiles');
        
        // creating the layers
        var grass = map.createStaticLayer('Grass', tiles, 0, 0);
        var obstacles = map.createStaticLayer('Obstacles', tiles, 0, 0);
        
        this.load.spritesheet('dude', 
        'images/dude.png',
        { frameWidth: 32, frameHeight: 48 }
    );
        
        // make all tiles in obstacles collidable
        obstacles.setCollisionByExclusion([-1]);
        
        //  animation with key 'left', we don't need left and right as we will use one and flip the sprite
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('player', { frames: [1, 7, 1, 13]}),
            frameRate: 10,
            repeat: -1
        });
        
        // animation with key 'right'
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('player', { frames: [1, 7, 1, 13] }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'up',
            frames: this.anims.generateFrameNumbers('player', { frames: [2, 8, 2, 14]}),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'down',
            frames: this.anims.generateFrameNumbers('player', { frames: [ 0, 6, 0, 12 ] }),
            frameRate: 10,
            repeat: -1
        });      
        
        
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('player2', { frames: [1, 7, 1, 13]}),
            frameRate: 10,
            repeat: -1
        });
        
        // animation with key 'right'
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('player2', { frames: [1, 7, 1, 13] }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'up',
            frames: this.anims.generateFrameNumbers('player2', { frames: [2, 8, 2, 14]}),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'down',
            frames: this.anims.generateFrameNumbers('player2', { frames: [ 0, 6, 0, 12 ] }),
            frameRate: 10,
            repeat: -1
        });  

        // our player sprite created through the phycis system
        this.player = this.physics.add.sprite(50, 100, 'player', 6);
        this.player2 = this.physics.add.sprite(100, 100, 'player2', 6);
        
        // don't go out of the map
        this.physics.world.bounds.width = map.widthInPixels;
        this.physics.world.bounds.height = map.heightInPixels;
        this.player.setCollideWorldBounds(true);
        this.player2.setCollideWorldBounds(true);
        
        // don't walk on trees
        this.physics.add.collider(this.player, obstacles);
        this.physics.add.collider(this.player2, obstacles);
        // limit camera to map
        this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        this.cameras.main.startFollow(this.player);
        this.cameras.main.roundPixels = true; // avoid tile bleed
    
        // user input
        this.cursors = this.input.keyboard.createCursorKeys();
        
        // where the enemies will be
        this.spawns = this.physics.add.group({ classType: Phaser.GameObjects.Zone });
        for(var i = 0; i < 30; i++) {
            var x = Phaser.Math.RND.between(0, this.physics.world.bounds.width);
            var y = Phaser.Math.RND.between(0, this.physics.world.bounds.height);
            // parameters are x, y, width, height
            this.spawns.create(x, y, 20, 20);            
        }        
        // add collider
        this.physics.add.overlap(this.player, this.spawns, this.onMeetEnemy, false, this);
    },
    onMeetEnemy: function(player, zone) {        
        // we move the zone to some other location
        zone.x = Phaser.Math.RND.between(0, this.physics.world.bounds.width);
        zone.y = Phaser.Math.RND.between(0, this.physics.world.bounds.height);
        
        // shake the world
        this.cameras.main.shake(300);
        
        // start battle 
    },
    update: function (time, delta)
    {
            if (gameStarted == 4) {
    //    this.controls.update(delta);
    
        this.player.body.setVelocity(0);
        this.player2.body.setVelocity(0);

        // Horizontal movement
        if (left == 1 || this.cursors.left.isDown)
        {
            this.player.body.setVelocityX(-80);
        }
        else if (right == 1 || this.cursors.right.isDown)
        {
            this.player.body.setVelocityX(80);
        }
                
        if (left2 == 1)
        {
            this.player2.body.setVelocityX(-80);
        }
        else if (right2 == 1)
        {
            this.player2.body.setVelocityX(80);
        }

        // Vertical movement
        if (up == 1 || this.cursors.up.isDown)
        {
            this.player.body.setVelocityY(-80);
        }
        else if (down == 1 || this.cursors.down.isDown)
        {
            this.player.body.setVelocityY(80);
        }    
        
        if (up2 == 1)
        {
            this.player2.body.setVelocityY(-80);
        }
        else if (down2 == 1)
        {
            this.player2.body.setVelocityY(80);
        }  

        // Update the animation last and give left/right animations precedence over up/down animations
        if (left == 1 || this.cursors.left.isDown)
        {
            this.player.anims.play('left', true);
            this.player.flipX = true;
        }
        else if (right == 1 || this.cursors.right.isDown)
        {
            this.player.anims.play('right', true);
            this.player.flipX = false;
        }
        else if (up == 1 || this.cursors.up.isDown)
        {
            this.player.anims.play('up', true);
        }
        else if (down == 1 || this.cursors.down.isDown)
        {
            this.player.anims.play('down', true);
        }
        else
        {
            this.player.anims.stop();
        }
                
                
        if (left2 == 1)
        {
            this.player2.anims.play('left', true);
            this.player2.flipX = true;
        }
        else if (right2 == 1)
        {
            this.player2.anims.play('right', true);
            this.player2.flipX = false;
        }
        else if (up2 == 1)
        {
            this.player2.anims.play('up', true);
        }
        else if (down2 == 1)
        {
            this.player2.anims.play('down', true);
        }
        else
        {
            this.player2.anims.stop();
        }
        }
    }
    
});

var configRPG = {
    type: Phaser.AUTO,
    parent: gRPG,
    width: 320,
    height: 240,
    zoom: 2,
    pixelArt: true,
    audio: {
        disableWebAudio: false
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: true // set to true to view zones
        }
    },
    scene: [
        BootScene,
        WorldScene
    ]
};

var gameRPG = new Phaser.Game(configRPG);
        </script>        
        <style>
        html,
        body {
            margin: 0;
            padding: 0;
        }
    </style>
    </head>
    <body>
    </body>
</html>
