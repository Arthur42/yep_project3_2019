<?php

include 'php/user.php';

class Database
{
    private $_bdd = null;

    public function __construct() {
        try {
            $this->_bdd = new PDO('mysql:host=localhost:3308;dbname=airdyscovery;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
        }
    }
    
    public function getUserByPseudo($pseudo) {
        $user = new User;
        if ($pseudo == null)
            return $user;
        $request = $this->_bdd->prepare('SELECT * FROM user WHERE pseudo = :pseudo');
        try {
            $request->execute(array(':pseudo' => $pseudo));
            $data = $request->fetch();
            $user->setEmail($data['email']);
            $user->setPseudo($data['pseudo']);
            $user->setImage($data['image']);
            $user->setLanguage($data['language']);
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $user;
    }

    public function emailExist($email) {
        if ($email == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT id FROM user WHERE email = :email');

        try {
            $request->execute(array(':email' => $email));
            if ($data = $request->fetch()) {
                $status = true;
            }
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function pseudoExist($pseudo) {
        if ($pseudo == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT id FROM user WHERE pseudo = :pseudo');

        try {
            $request->execute(array(':pseudo' => $pseudo));
            if ($data = $request->fetch()) {
                $status = true;
            }
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function addUser($email, $pseudo, $password) {
        if ($email == null || $pseudo == null || $password == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('INSERT INTO user (`email`, `pseudo`, `password`, `image`, `language`) SELECT :email, :pseudo, :password, \'/yep_project3_2019/img/user.png\', \'fr\'');
        try {
            $request->execute(array(
                'email' => $email, 
                'pseudo' => $pseudo, 
                'password' => $password,
                ));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }

    public function getImageByPseudo($pseudo) {
        $image = '';
        if ($email == null)
            return $image;
        $request = $this->_bdd->prepare('SELECT image FROM user WHERE pseudo=:pseudo');
        
        try {
            $request->execute(array('pseudo' => $pseudo));
            $data = $request->fetch();
            $image = $data['image'];
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return $image;
    }
    
    public function userExist($email, $password) {
        if ($email == null || $password == null)
            return false;
        $status = false;
        $request = $this->_bdd->prepare('SELECT id FROM user WHERE email=:email AND password=:password');
        
        try {
            $request->execute(array('email' => $email,
                                   'password' => $password));
            if ($request->fetch())
                $status = true;
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function socketSetWebConnexion($email, $webID) {
        if ($email == null || $webID == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('INSERT INTO socketConnexion (`email`, `webID`) SELECT :email, :webID');
        try {
            $request->execute(array(
                'email' => $email, 
                'webID' => $webID,
                ));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function socketSetMobileConnexion($webID, $mobileID) {
        if ($webID == null || $mobileID == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE socketConnexion SET mobileID=:mobileID WHERE webID=:webID');
        try {
            $request->execute(array(
                'webID' => $webID,
                'mobileID' => $mobileID, 
                ));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function socketDeleteConnexion($webID) {
        if ($webID == null)
            return false;
        $request = $this->_bdd->prepare('DELETE FROM socketConnexion WHERE webID=:webID');
        
        try {
            $request->execute(array('webID' => $webID));
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return true;
    }
    
    public function socketEmptyConnexions() {
        $request = $this->_bdd->prepare('DELETE FROM socketConnexion');
        
        try {
            $request->execute();
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
        } finally {
            $request->closeCursor();
        }
        return true;
    }
    
    public function getWebIDWithMobileID($mobileID) {
        if ($mobileID == null)
            return 0;
        $webID = 0;
        $request = $this->_bdd->prepare('SELECT webID FROM socketConnexion WHERE mobileID=:mobileID');
        
        try {
            $request->execute(array('mobileID' => $mobileID));
            $data = $request->fetch();
            $webID = $data["webID"];
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $webID = 0;
        } finally {
            $request->closeCursor();
        }
        return $webID;
    }
    
    public function joinRpg($email) {
        if ($email == null)
            return false;
        $status = true;
        $request = $this->_bdd->prepare('UPDATE socketConnexion SET playRPG=1 WHERE email=:email');
        try {
            $request->execute(array(
                'email' => $email,
                ));
        } catch (Exception $e) {
            echo 'Error ' . $e->getMessage();
            $status = false;
        } finally {
            $request->closeCursor();
        }
        return $status;
    }
    
    public function isUserPlayingRpg($webID) {
        if ($webID == null)
            return 0;
        $playRpg = 0;
        $request = $this->_bdd->prepare('SELECT playRPG FROM socketConnexion WHERE webID=:webID');
        
        try {
            $request->execute(array('webID' => $webID));
            if ($data = $request->fetch())
                $playRpg = $data["playRPG"];
            echo "playrpg  : $playRpg\n";
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $playRpg = 0;
        } finally {
            $request->closeCursor();
        }
        return $playRpg;
    }
    
    public function getRPGSecondUserID($webID) {
        if ($webID == null)
            return 0;
        $newWebID = 0;
        $request = $this->_bdd->prepare('SELECT webID FROM socketConnexion WHERE webID!=:webID');
        
        try {
            $request->execute(array('webID' => $webID));
            if ($data = $request->fetch())
                $newWebID = $data["webID"];
            echo "second RPG user : $newWebID";
        } catch (Execption $e) {
            echo 'Error ' . $e->getMessage();
            $newWebID = 0;
        } finally {
            $request->closeCursor();
        }
        return $newWebID;
    }
}
?>
