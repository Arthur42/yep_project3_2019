<?php
    class User {
        private $_email = '';
        private $_pseudo = '';
        private $_image = '';
        private $_language = '';

        public function getEmail() {
            return $this->_email;
        }

        public function setEmail($email) {
            $this->_email = $email;
        }

        public function getPseudo() {
            return $this->_pseudo;
        }

        public function setPseudo($pseudo) {
            $this->_pseudo = $pseudo;
        }

        public function getImage() {
            return $this->_image;
        }

        public function setImage($image) {
            $this->_image = $image;
        }

        public function getLanguage() {
            return $this->_language;
        }

        public function setLanguage($language) {
            $this->_language = $language;
        }
    }
?>