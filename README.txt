installation :

- install composer
- in a terminal type : composer install
- install ngrok and put the exec file here
- create a airdyscovery database on mysql
- load database/airdyscovery.sql into airdyscovery

usage :

in two terminals type :
- php app.php
- ngrok http 8080