User-agent: *
Disallow: /css/
Disallow: /database/
Disallow: /img/
Disallow: /js/
Disallow: /lib/
Disallow: /php/