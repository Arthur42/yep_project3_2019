<?php

namespace MyApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

include __DIR__ . "/../database/database.php";
use Database;

function getStr($msg, $command) {
    if (substr_compare($msg, $command, 1, strlen($command)) == 0) {
        return substr($msg, strlen($command) + 1);
    }
    return null;
}

class Socket implements MessageComponentInterface {
    private $_bdd = null;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->_bdd = new Database;
        $this->_bdd->socketEmptyConnexions();
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "New connection : ({$conn->resourceId}).\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        foreach ( $this->clients as $client ) {

            if ( $from->resourceId == $client->resourceId ) {
                if (($data = getStr($msg, "webConnexion:")) != null) {
                    echo "$from->resourceId wait for mobile connexion.\n";
                    $this->_bdd->socketSetWebConnexion($data, $from->resourceId);
                    $client->send( "mobileID:$from->resourceId" );
                } elseif (($data = getStr($msg, "mobileConnexion:")) != null) {
                    if ($this->_bdd->socketSetMobileConnexion($data, $from->resourceId)) {
                        echo "mobile ($from->resourceId) conneted to web ($data)\n";
                        $this->sendTo("mobileConnected", $data);
                    }
                } elseif (($data = getStr($msg, "mobileButton:")) != null) {
                    $webID = $this->_bdd->getWebIDWithMobileID($from->resourceId);
                    $this->sendTo("action:" . $data, $webID);
                    if ($this->_bdd->isUserPlayingRpg($webID) == 1
                       && ($secondWebUser = $this->_bdd->getRPGSecondUserID($webID)) != 0) {
                        echo "second user : $secondWebUser \n";
                        $this->sendTo("action-player2:". $data, $secondWebUser);
                    }
                    echo "mobile ($from->resourceId) send to web ($webID) : $data\n";
                } else if (($data = getStr($msg, "rpgJoin:")) != null) {
                    $this->_bdd->joinRpg($data);
                    echo "($from->resourceId) join the RPG game.\n";
                } else
                    echo "No command found : $msg\n";
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->_bdd->socketDeleteConnexion($conn->ressourceId);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
    }
    
    private function sendTo($msg, $id) {
        foreach ( $this->clients as $client ) {
            if ($id == $client->resourceId) {
                $client->send($msg);
            }
        }
    }
}