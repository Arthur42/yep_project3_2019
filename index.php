<?php session_start(); ?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <?php include('views\include.php'); ?>
    </head>
    <body>
        <script src="js/phaser.min.js"></script>
        <script>
            var gameStarted = 0;
            var left = 0;
            var right = 0;
            var up = 0;
            var down = 0;
            var left2 = 0;
            var right2 = 0;
            var up2 = 0;
            var down2 = 0;
        </script>
        <?php $currentPage = 'Home'; ?>
        <link rel="stylesheet" type="text/css" href="css/welcome.css" >
        <link rel="stylesheet" type="text/css" href="css/lightMode.css">
        <div id="main-wrapper">
            <?php  include('views/header.php'); ?>
            <div class="content" id="connectionID" style="display:none;">
                <div class="case-connection">
                    <p>Code de connexion mobile :</p>
                    <p id="connectionCode"></p>
                </div>
            </div>
            <div class="content" id="content">
                <?php $margin = 5; ?>
                <?php include('views/game_preview.php'); ?>
                <?php $margin = 0; ?>
            </div>
            <div id="gNotMario" style="display:none;">
                <?php include('php/NotMario/app.php'); ?>
            </div>
            <div id="gBreakout" style="display:none;">
                <?php include('php/Breakout/index.php'); ?>
            </div>
            <div id="gRPG" style="display:none;">
                <?php include('php/RPG_2/index.php'); ?>
            </div>
            <?php include('views/footer.php'); ?>
        </div>
    </body>
</html>

<script type="text/javascript">
    const [red, green, blue] = [255, 255, 255]
    const [red1, green1, blue1] = [174, 183, 101]
    const content = document.querySelector('.content')

    var dom = document.getElementById('content')

    const viewportHeight = window.innerHeight
    const elementOffsetTop = dom.offsetTop

    var distance = viewportHeight - elementOffsetTop

    var height = document.getElementById('content').getBoundingClientRect().height
    var footer = document.getElementById('footer').getBoundingClientRect().height

    height = height - distance + footer

    window.addEventListener('scroll', () => {
        var y = window.scrollY / height
        const [r, g, b] = [red-(174*y), green-(183*y), blue-(101*y)].map(Math.round)
        dom.style.backgroundImage = `linear-gradient(to bottom, rgb(255, 255, 255), rgb(${r}, ${g}, ${b}))`
    })
</script>
<?php
if (isset($_SESSION['user'])) {
?>
<script>
    var socket  = new WebSocket('ws://localhost:8080');
    function transmitMessage(message) {
        socket.send( message );
    }
    socket.onmessage = function(e) {
        var response = e.data;
        console.log( "message : " , response );
        var key2 = response.substring(15);
        if (response.indexOf("action:") == 0) {
            var key = response.substring(7);
            if (key.indexOf("left-up") == 0) {
                left = 0;
            } else if (key.indexOf("left-down") == 0) {
                left = 1;
                if (gameStarted == 0) {
                    changeSelectedGame(-1);
                }
            } else if (key.indexOf("up-up") == 0) {
                up = 0;
            } else if (key.indexOf("up-down") == 0) {
                up = 1;
            } else if (key.indexOf("right-up") == 0) {
                right = 0;
            } else if (key.indexOf("right-down") == 0) {
                right = 1;
                if (gameStarted == 0) {
                    changeSelectedGame(1);
                }
            } else if (key.indexOf("down-up") == 0) {
                down = 0;
            } else if (key.indexOf("down-down") == 0) {
                down = 1;
            } else if (key.indexOf("a-up") == 0) {
                if (gameStarted == 0) {
                    switch (selectGame) {
                        case 1:
                            gameStarted = selectGame;
            (document.getElementById('content')).style.display = 'none';
            (document.getElementById('gNotMario')).style.removeProperty('display');
                            gameNotMario.scene.start("default");
                            break;
                        case 2:
                            gameStarted = selectGame;
            (document.getElementById('content')).style.display = 'none';
            (document.getElementById('gBreakout')).style.removeProperty('display');
                            gameNotMario.scene.start("default");
                            break;
                        case 4:
                            gameStarted = selectGame;
            (document.getElementById('content')).style.display = 'none';
            (document.getElementById('gRPG')).style.removeProperty('display');
            transmitMessage("_rpgJoin:<?php echo $_SESSION['user']; ?>");
                            break;
                    }
                }
            } else if (key.indexOf("a-down") == 0) {
                
            } else if (key.indexOf("b-up") == 0) {
                if (gameStarted != 0) {
                    gameStarted = 0;
            (document.getElementById('content')).style.removeProperty('display');
            (document.getElementById('gNotMario')).style.display = 'none';
            (document.getElementById('gBreakout')).style.display = 'none';
            (document.getElementById('gRPG')).style.display = 'none';
                }
            } else if (key.indexOf("b-down") == 0) {
                
            }
        } else if (response.indexOf("action-player2:") == 0) {
            console.log( "player 2 : " , key2 );
            if (key2.indexOf("left-up") == 0) {
                left2 = 0;
            } else if (key2.indexOf("left-down") == 0) {
                left2 = 1;
            } else if (key2.indexOf("up-up") == 0) {
                up2 = 0;
            } else if (key2.indexOf("up-down") == 0) {
                up2 = 1;
            } else if (key2.indexOf("right-up") == 0) {
                right2 = 0;
            } else if (key2.indexOf("right-down") == 0) {
                right2 = 1;
            } else if (key2.indexOf("down-up") == 0) {
                down2 = 0;
            } else if (key2.indexOf("down-down") == 0) {
                down2 = 1;
            }
        } else if (response.indexOf("mobileID:") == 0) {
            (document.getElementById('connectionID')).style.removeProperty('display');
            (document.getElementById('content')).style.display = 'none';
            var code = response.substring(9);
            (document.getElementById('connectionCode')).innerHTML = code;
            gameNotMario.scene.pause("default");
        } else if (response.localeCompare("mobileConnected") == 0) {
            (document.getElementById('connectionID')).style.display = 'none';
            (document.getElementById('content')).style.removeProperty('display');
        } else {
            alert( response );
        }
    }
    socket.onopen = function (e) {
        transmitMessage("_webConnexion:<?php echo $_SESSION['user']; ?>");
    }
</script>
<style>
    .case-connection {
        padding: 20px;
        margin: 20px auto 20px auto;
        background-color: bisque;
        width: 350px;
    }
</style>
<?php
}
?>